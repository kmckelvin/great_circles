#!/usr/bin/env ruby
require_relative "../lib/great_circles"

require 'optparse'

options = {}
optparser = OptionParser.new do |opts|
  opts.banner = "Usage: find_customers -f [CUSTOMERS_FILE] -d [DISTANCE]"
  opts.on("-f", "--file [FILE]", "Path to the file that contains the customers") do |path|
    options[:file_path] = path
  end

  opts.on("-d", "--distance [DISTANCE]", "The radius in km to search from the centre point") do |distance|
    options[:distance] = distance
  end

  opts.on("-h", "--help", "Display this help screen") do
    puts opts
    exit
  end
end
optparser.parse!

begin
  file_path = options.fetch(:file_path)
  distance = GreatCircles::Distance.new(km: Float(options.fetch(:distance)))
rescue KeyError, ArgumentError
  puts optparser
  exit 1
end

customers_jsons = File.read(file_path).split("\n")
customers = GreatCircles::CustomerParser.new.parse_all(customers_jsons)

intercom_office = GreatCircles::Coordinate.from_degrees(53.3381985, -6.2592576)

finder = GreatCircles::RangeFinder.new(customers, intercom_office)
customers_in_range = finder.find_within(distance)

customers_in_range.sort_by{ |c| c.user_id }.each do |customer|
  puts "name: #{customer.name}, user_id: #{customer.user_id}"
end
