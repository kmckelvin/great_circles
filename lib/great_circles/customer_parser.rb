require 'json'

module GreatCircles
  class CustomerParser
    def parse_one(json)
      customer_attributes = JSON.parse(json)
      Customer.new(customer_attributes)
    end

    def parse_all(jsons)
      jsons.map { |json| parse_one(json) }
    end
  end
end
