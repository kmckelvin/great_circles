module GreatCircles
  class SphericalDistanceCalculator
    EARTH_RADIUS = Distance.new(km: 6371)

    def distance_between(point_a, point_b, sphere_radius)
      # Great-circle distance formula determines the shortest distance between two
      # points on the surface of a sphere.
      # https://en.wikipedia.org/wiki/Great-circle_distance
      arc = Math.acos((Math.sin(point_a.latitude)*Math.sin(point_b.latitude)) + (Math.cos(point_a.latitude)*Math.cos(point_b.latitude)*Math.cos(point_b.longitude-point_a.longitude)))

      Distance.new(nm: sphere_radius.nm * arc)
    end
  end
end
