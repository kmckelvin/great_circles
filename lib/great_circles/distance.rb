module GreatCircles
  class Distance
    include Comparable

    NM_UNIT_CONVERSIONS = {
      nm: ->(d) { d },
      cm: ->(d) { d * 10**7 },
      m: ->(d) { d * 10**9 },
      km: ->(d) { d * 10**12 },
    }.freeze

    def initialize(distance)
      @nm = 0
      NM_UNIT_CONVERSIONS.each do |unit, conversion|
        @nm += conversion.(distance.delete(unit)) if distance.has_key?(unit)
      end

      raise ArgumentError, "Invalid units were given: #{distance.keys}" if distance.any?
    end

    def ==(other)
      other.nm == nm
    end

    def <=>(other)
      nm <=> other.nm
    end

    attr_reader :nm
  end
end
