module GreatCircles
  Coordinate = Struct.new(:latitude, :longitude) do
    def self.from_degrees(latitude, longitude)
      new(degrees_to_radians(latitude), degrees_to_radians(longitude))
    end

    def distance_to(coordinate, sphere_radius)
      calculator = SphericalDistanceCalculator.new
      calculator.distance_between(self, coordinate, sphere_radius)
    end

    private_class_method
    def self.degrees_to_radians(degrees)
      (degrees * Math::PI) / 180
    end
  end
end
