require 'virtus'

module GreatCircles
  class Customer
    include Virtus.value_object

    values do
      attribute :latitude, BigDecimal
      attribute :longitude, BigDecimal
      attribute :user_id, Integer
      attribute :name, String
    end

    def coordinate
      Coordinate.from_degrees(latitude, longitude)
    end
  end
end
