module GreatCircles
  class RangeFinder
    def initialize(customers, center)
      @customers = customers
      @center = center
    end

    def find_within(distance)
      customers.select { |c|
        c.coordinate.distance_to(center, SphericalDistanceCalculator::EARTH_RADIUS) < distance
      }
    end

    private
    attr_reader :customers, :center
  end
end
