# Great Circles

This is an exercise that calculates great-circle distance between points on
a sphere to find customers in a sample set that lie within a given radius of
a central point (Dublin by default).

### Usage

To find customers within 100km of Dublin

```
bin/find_customers.rb -f bin/gistfile1.txt -d 100
```

Use the `-h` or `--help` flag to print the help screen.

```
Usage: find_customers -f [CUSTOMERS_FILE] -d [DISTANCE]
    -f, --file [FILE]                Path to the file that contains the customers
    -d, --distance [DISTANCE]        The radius in km to search from the centre point
    -h, --help                       Display this help screen
```

### Sample output

```
$ bin/find_customers.rb -f bin/gistfile1.txt -d 100
name: Ian Kehoe, user_id: 4
name: Nora Dempsey, user_id: 5
name: Theresa Enright, user_id: 6
name: Eoin Ahearn, user_id: 8
name: Richard Finnegan, user_id: 11
name: Christina McArdle, user_id: 12
name: Olive Ahearn, user_id: 13
name: Michael Ahearn, user_id: 15
name: Patricia Cahill, user_id: 17
name: Eoin Gallagher, user_id: 23
name: Rose Enright, user_id: 24
name: Stephen McArdle, user_id: 26
name: Oliver Ahearn, user_id: 29
name: Nick Enright, user_id: 30
name: Alan Behan, user_id: 31
name: Lisa Ahearn, user_id: 39
```
