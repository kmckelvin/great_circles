require 'spec_helper'

module GreatCircles
  RSpec.describe RangeFinder do
    describe "#find_customers" do
      it "finds customers within a given range of a central point" do
        all_customers = [
          Customer.new(latitude: 53.038056, user_id: 26, name: "Stephen McArdle", longitude: -7.653889),
          Customer.new(latitude: 33.038056, user_id: 27, name: "John Doe", longitude: -7.653889),
        ]

        office = Coordinate.from_degrees(53.3381985, -6.2592576)

        finder = RangeFinder.new(all_customers, office)
        customers_in_range = finder.find_within(Distance.new(km: 100))

        expect(customers_in_range).to eq [all_customers[0]]
      end
    end
  end
end
