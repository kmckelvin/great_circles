require 'spec_helper'

module GreatCircles
  RSpec.describe Coordinate do
    it "holds a latitude and longitude" do
      coordinate = Coordinate.new(1, 2)
      expect(coordinate.latitude).to eq 1
      expect(coordinate.longitude).to eq 2
    end

    describe ".from_degrees" do
      it "converts the arguments from degrees to radians" do
        coordinate = Coordinate.from_degrees(90, 180)
        expect(coordinate.latitude).to eq Math::PI / 2
        expect(coordinate.longitude).to eq Math::PI
      end
    end

    describe "#distance_to" do
      context "given another coordinate" do
        it "returns the Great-circle distance to the other coordinate" do
          johannesburg = Coordinate.from_degrees(-26.2044, 28.0456)
          durban = Coordinate.from_degrees(-29.8833, 31.05)

          distance = johannesburg.distance_to(durban, SphericalDistanceCalculator::EARTH_RADIUS)

          expect(distance).to eq Distance.new(km: 504.20137876862316)
        end
      end
    end
  end
end
