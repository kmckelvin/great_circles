require 'spec_helper'

module GreatCircles
  RSpec.describe Customer do
    describe "#coordinate" do
      it "returns the customer's coordinate" do
        customer = Customer.new(latitude: 30, longitude: 40)
        expected = Coordinate.from_degrees(30, 40)

        expect(customer.coordinate).to eq expected
      end
    end
  end
end
