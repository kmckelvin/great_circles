require 'spec_helper'

module GreatCircles
  RSpec.describe SphericalDistanceCalculator do
    describe "#distance_between" do
      context "with the coordinates of Johannesburg and Durban" do
        it "returns the arc line distance between the two coordinates" do
          calculator = SphericalDistanceCalculator.new
          johannesburg = Coordinate.from_degrees(-26.2044, 28.0456)
          durban = Coordinate.from_degrees(-29.8833, 31.05)

          distance = calculator.distance_between(johannesburg, durban, SphericalDistanceCalculator::EARTH_RADIUS)

          expect(distance).to eq Distance.new(km: 504.20137876862316)
        end
      end
    end
  end
end
