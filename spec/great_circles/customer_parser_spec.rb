require 'spec_helper'

module GreatCircles
  RSpec.describe CustomerParser do
    describe "#parse_one" do
      context "given a valid JSON string" do
        it "creates a Customer object" do
          json = '{"latitude": "53.038056", "user_id": 26, "name": "Stephen McArdle", "longitude": "-7.653889"}'
          expected = Customer.new(latitude: 53.038056, user_id: 26, name: "Stephen McArdle", longitude: -7.653889)
          parser = CustomerParser.new

          customer = parser.parse_one(json)

          expect(customer).to eq expected
        end
      end
    end

    describe "#parse_all" do
      context "given an array of JSON strings" do
        it "returns an array of Customer objects" do
          jsons = [
            '{"latitude": "53.038056", "user_id": 26, "name": "Stephen McArdle", "longitude": "-7.653889"}',
            '{"latitude": "53", "user_id": 13, "name": "Olive Ahearn", "longitude": "-7"}'
          ]
          expected = [
            Customer.new(latitude: 53.038056, user_id: 26, name: "Stephen McArdle", longitude: -7.653889),
            Customer.new(latitude: 53, longitude: -7, name: "Olive Ahearn", user_id: 13)
          ]
          parser = CustomerParser.new
          customers = parser.parse_all(jsons)

          expect(customers).to eq expected
        end
      end
    end
  end
end
