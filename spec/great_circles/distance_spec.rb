require 'spec_helper'

module GreatCircles
  RSpec.describe Distance do
    context "construction" do
      it "constructs from nm" do
        distance = Distance.new(nm: 1234)
        expect(distance.nm).to eq 1234
      end

      it "constructs from km" do
        base_distance = Distance.new(m: 1000)
        distance = Distance.new(km: 1)

        expect(distance).to eq base_distance
      end

      it "constructs from m" do
        base_distance = Distance.new(cm: 100)
        distance = Distance.new(m: 1)

        expect(distance).to eq base_distance
      end

      it "raises an error if invalid units are given" do
        expect {
          Distance.new(invalid: 1)
        }.to raise_error(ArgumentError)
      end
    end

    context "comparison" do
      describe "equality" do
        it "returns true if two distances are equal" do
          expect(Distance.new(m: 1)).to eq Distance.new(cm: 100)
        end

        it "returns false if two distances are not equal" do
          expect(Distance.new(m: 1)).not_to eq Distance.new(cm: 101)
        end
      end

      describe "greater/less than" do
        it "returns true if left distance is greater than right distance" do
          expect(Distance.new(m: 2) > Distance.new(m: 1)).to be_truthy
        end

        it "returns false if left distance is less than right distance" do
          expect(Distance.new(m: 1) > Distance.new(m: 2)).to be_falsey
        end
      end
    end
  end
end
